#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/timex.h>
#include <sysexits.h>

int main(int _argc, char** _argv)
{
	int opts = 0;
    struct option longopts[] =
	{
		{ "offset",   no_argument, NULL, 'a' },
		{ "freq",     no_argument, NULL, 'b' },
		{ "maxerror", no_argument, NULL, 'c' },
		{ 0, 0, 0, 0 }
	};
    int ret = 0;
	struct timex buf;

    if (_argc != 2)
		exit(EX_USAGE);

	memset(&buf, 0, sizeof buf);

	ret = ntp_adjtime(&buf);
	if (ret == -1)
	{
		fprintf(stderr, "ntp_adjtime: %s\n", strerror(errno));
		exit(EX_OSERR);
	}

	while ((opts = getopt_long(_argc, _argv, "abc", longopts, NULL)) != -1)
	{
		switch (opts)
		{
			case 'a':
				// ns to s
				printf("%.9lf\n", buf.offset / 1000000000.0);
				goto out;
			case 'b':
				// ppm
				printf("%.4lf\n", buf.freq / 65536.0);
				goto out;
			case 'c':
				// us to s
				printf("%.6lf\n", buf.maxerror / 1000000.0);
				goto out;
			default:
				exit(EX_USAGE);
		}
	}

out:
	exit(EX_OK);
}

